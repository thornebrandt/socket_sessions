const fs = require('fs');
const express = require('express');
const app = express();
const config = require('./config.js');
const cors = require('cors');

let ssl_options;

if (config.local) {
    ssl_options = {
        key: fs.readFileSync('certs/server.key'),
        cert: fs.readFileSync('certs/server.crt')
    }
} else {
    ssl_options = {
        key: fs.readFileSync(config.certPath + 'key.pem'),
        cert: fs.readFileSync(config.certPath + 'cert.pem'),
        ca: fs.readFileSync(config.certPath + 'chain.pem'),
    }
}

var port;
var io;
let clientSessions = {};

const processDelay = 10000;


app.use(express.json({ limit: '50mb' }));
app.use(express.static("public"));
app.use(cors({
    origin: 'https://experiments.thornebrandt.com'
}));

async function main() {
    await setupHttpsServer();
    setupSocketServer();
}


async function setupHttpsServer() {
    var https = require('https');
    port = process.env.PORT || 8080;

    var serverHttps = https.createServer(ssl_options, app).listen(port, () => {
        console.log("listening on " + port);
    });

    io = require("socket.io")({
        cors: {
            origin: "https://experiments.thornebrandt.com",
            credentials: true,
        },
        allowEIO3: true
    }).listen(serverHttps);
}


function shuffleString(str) {
    // Convert the string to an array of characters
    const chars = str.split('');

    // Perform the Fisher-Yates shuffle
    for (let i = chars.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [chars[i], chars[j]] = [chars[j], chars[i]];
    }

    // Convert the shuffled array back to a string
    const shuffledStr = chars.join('');
    return shuffledStr;
}

function isEmpty(value) {
    return (value == null || value == undefined || value == "undefined" ||
        value == "null" || (typeof value === "string" && value.trim().length === 0));
}

function createOriginalClientSession(client_id) {
    clientSessions[client_id] = {
        socket_id: client_id,
        client_id: client_id,
        state: "new"
    }
    //client id should be "original_socket_id
}

function checkIfSessionExists(client) {
    console.log("checking if session exists.");
    let client_id = client.handshake.query.client_id;
    if (!isEmpty(client_id)) {
        console.log("Client id exists: " + client_id);
        if (!clientSessions[client_id]) {
            createOriginalClientSession(client_id);
        }
    } else {
        client_id = client.id;
        console.log("client id does not exist, creating one with id: " + client.id);
        createOriginalClientSession(client_id);

    }
    //reset socket id for existing clients. 
    clientSessions[client_id].socket_id = client.id; //replace socket.id with actual socket id so we can look it up later.  
    console.log("createed clientSession: ", clientSessions[client_id]);
    return clientSessions[client_id];
}

function connectWithCorrectClient(client_key) {
    for (let key in clientSessions) {
        if (key === client_key) {
            return clientSessions[key];
        }
    }
}

function processNameAfterDelay(io, client, clientSession) {
    //client is the socket. 
    //client session is the stored object.

    clientSession.state = "processing";
    clientSession.name_requested = new Date().getTime();
    console.log("starting processing for ", clientSession);
    client.emit(
        "processing",
        {

            ...clientSession
        }
    );
    setTimeout(() => {
        //we need to redefine clientSession because it might be out of scope. 


        clientSession = connectWithCorrectClient(clientSession.client_id);
        clientSession.server_name = shuffleString(clientSession.name);
        clientSession.state = "ready";
        console.log("finished, processing, but do we have the client? :", clientSession);
        io.to(clientSession.socket_id).emit(
            "name_processed",
            {
                ...clientSession
            }
        );
    }, processDelay);
}

function setupSocketServer() {
    console.log("setting up socket server");
    io.on("connection", (client) => {

        let clientSession = checkIfSessionExists(client);

        if (!isEmpty(clientSession.name)) {
            if (!clientSession.server_name) {
                if (clientSession.state === "processing") {
                    //do nothing, the processed name will be here soon. 
                } else {
                    //clientSession.processedName = shuffleString(clientSession.name);
                    console.log("something went wrong. we have a name but we never started processing it. re-processing");
                    processNameAfterDelay(io, client, clientSession);
                }
            }
            client.emit(
                "welcome_back",
                {
                    ...clientSession,
                    socket_id: client.id,
                }
            )
        } else {
            client.emit(
                "introduction",
                {
                    ...clientSession,
                    message: "save this socket id in your browser.",
                }
            );
        }

        client.on("connect_failed", (err) => {
            console.log("connect failed!", err);
        });

        client.on("error", (err) => {
            console.log("there was an error on the connection!", err);
        });

        client.on("submit_name", (data) => {
            console.log(data);
            clientSession.name = data.name;
            processNameAfterDelay(io, client, clientSession);
        });
    });
}

main();






