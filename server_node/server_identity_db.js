const fs = require('fs');
const express = require('express');
const app = express();
const config = require('./config.js');
const cors = require('cors');
const mysql = require('mysql');

const mysql_connection = mysql.createConnection({
    host: config.mysql_host,
    user: config.mysql_user,
    password: config.mysql_password,
    database: config.mysql_database
});

// Connect to the database
mysql_connection.connect((err) => {
    if (err) {
        console.error('Error connecting to the database:', err);
        return;
    }
    console.log('Connected to the database!');
});

let ssl_options;

if (config.local) {
    ssl_options = {
        key: fs.readFileSync('certs/server.key'),
        cert: fs.readFileSync('certs/server.crt')
    }
} else {
    ssl_options = {
        key: fs.readFileSync(config.certPath + 'key.pem'),
        cert: fs.readFileSync(config.certPath + 'cert.pem'),
        ca: fs.readFileSync(config.certPath + 'chain.pem'),
    }
}

var port;
var io;
const processDelay = 6000;


app.use(express.json({ limit: '50mb' }));
app.use(express.static("public"));
app.use(cors({
    origin: 'https://experiments.thornebrandt.com'
}));

async function main() {
    await setupHttpsServer();
    setupSocketServer();
}


async function setupHttpsServer() {
    var https = require('https');
    port = process.env.PORT || 8080;

    var serverHttps = https.createServer(ssl_options, app).listen(port, () => {
        console.log("listening on " + port);
    });

    io = require("socket.io")({
        cors: {
            origin: "https://experiments.thornebrandt.com",
            credentials: true,
        },
        allowEIO3: true
    }).listen(serverHttps);
}


function shuffleString(str) {
    // Convert the string to an array of characters
    const chars = str.split('');

    // Perform the Fisher-Yates shuffle
    for (let i = chars.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [chars[i], chars[j]] = [chars[j], chars[i]];
    }

    // Convert the shuffled array back to a string
    const shuffledStr = chars.join('');
    return shuffledStr;
}

function isEmpty(value) {
    return (value == null || value == undefined || value == "undefined" ||
        value == "null" || (typeof value === "string" && value.trim().length === 0));
}

function setupSocketServer() {
    console.log("setting up socket server");
    io.on("connection", (client) => {
        console.log("about to do test create a nested client promise");
        findClientSession(client)
            .then(clientSession => {
                console.log("found clientSession, ", clientSession);

                client.on("connect_failed", (err) => {
                    console.log("connect failed!", err);
                });

                client.on("error", (err) => {
                    console.log("there was an error on the connection!", err);
                });

                client.on("submit_name", (payload) => {
                    console.log("just received a subbmitted name " + payload.name + " from " + payload.client_id + " uh " + payload.server_id);
                    clientSession.name = payload.name;
                    processNameAfterDelay(io, client, clientSession);
                });
            })
            .catch(error => {
                console.log("should have already been handled: ", error);
            })
    });
}

function checkIfSessionExists(client) {
    return new Promise((resolve, reject) => {
        try {
            let locally_stored_client_id = client.handshake.query.client_id;
            console.log("couldn't find " + locally_stored_client_id);
            if (!isEmpty(locally_stored_client_id)) {
                console.log("locally stored client id exists: " + locally_stored_client_id);
                const clientSession = {
                    client_id: locally_stored_client_id,
                    server_id: client.id,
                    found: true
                };
                resolve(clientSession);
            } else {
                console.log("no locally stored client id yet.");
                const clientSession = {
                    client_id: client.id
                };
                resolve(clientSession);
            }
        } catch (e) {
            reject(e);
        }
    });
}


function findClientSession(client) {
    const current_socket_id = client.id;
    return new Promise((resolve, reject) => {
        checkIfSessionExists(client)
            .then(clientSession => {
                if (clientSession.found) {
                    return getClientSession(clientSession.client_id);
                }
                return clientSession;
            })
            .then(clientSession => {
                if (clientSession.found) {
                    console.log("we are welcoming back a clientSession: ", clientSession);
                    client.emit(
                        "welcome_back",
                        {
                            ...clientSession,
                            message: "Welcome back. "
                        }
                    );
                    return clientSession;
                } else {
                    client.emit(
                        "introduction",
                        {
                            ...clientSession,
                            message: "Nice to meet you. save this id to your local or session storage"
                        }
                    );
                    return createClientSession(current_socket_id);
                }
            })
            .then(clientSession => {
                if (clientSession.server_id !== current_socket_id) {
                    console.log(clientSession.server_id + " does not match " + client.id + " updating.");
                    clientSession.server_id = current_socket_id;
                    return resolve(updateClientSessionServerID(clientSession));
                } else {
                    console.log("server socket id is still correct:  " + current_socket_id);
                    return resolve(clientSession);
                }
            })
            .catch(error => {
                console.log("error updating socket id", error);
                reject(error);
            });
    });
}


function createClientSession(client_id) {
    return new Promise((resolve, reject) => {
        const clientSession = {
            client_id: client_id,
            server_id: client_id,
            state: "new",
            last_image: null,
            name: null,
            server_name: null
        };
        const dateTime = new Date().getTime();
        const sql_query = `
            INSERT INTO client_sessions (
                created, client_id, server_id, 
                state, last_used, last_image, 
                last_processed_image, name, server_name
            )
            VALUES (NOW(), '${clientSession.client_id}', '${clientSession.server_id}', '${clientSession.state}', NOW(), NULL, NULL, NULL, NULL)
        `;
        mysql_connection.query(sql_query, (error, result) => {
            if (error) {
                reject(error);
            } else {
                resolve(clientSession);
            }
        });
    });
}

function getClientSession(client_id) {
    return new Promise((resolve, reject) => {
        if (isEmpty(client_id)) {
            resolve({});
        }
        const sql_query = `SELECT * FROM client_sessions WHERE client_id = '${client_id}'`;
        mysql_connection.query(sql_query, (error, results) => {
            if (error) {
                console.error('Error querying the database:', error);
                reject(error);
            } else {
                if (results.length === 0) {
                    // If no matching row found, resolve with empty object.
                    // clientSession.found will be false. 
                    return resolve({});
                } else {
                    // Extract the first row from the results and return it as an object
                    const clientSession = results[0];
                    clientSession.found = true; //local information. 
                    return resolve(clientSession);
                }
            }
        });
    });
}


function updateClientSessionServerID(clientSession) {
    //update just the socket id, between processes. 
    return new Promise((resolve, reject) => {
        const sql_query = `
            UPDATE client_sessions
            SET server_id = '${clientSession.server_id}',
            last_used = NOW() 
            WHERE client_id = '${clientSession.client_id}'
            `;
        mysql_connection.query(sql_query, (error, results) => {
            if (error) {
                reject(error);
            } else {
                if (results.length === 0) {
                    return reject("no results updated");
                } else {
                    console.log("received results after updating serverID ", results, clientSession);
                    return resolve(clientSession);
                }
            }
        });
    });
}


function updateClientSession(clientSession) {
    //update the entire client session, after processing. 
    return new Promise((resolve, reject) => {
        const sql_query = `
            UPDATE client_sessions
            SET server_id = '${clientSession.server_id}',
            state = '${clientSession.state}',
            name = '${clientSession.name}',
            server_name = '${clientSession.server_name}',
            last_used = NOW() 
            WHERE client_id = '${clientSession.client_id}'
            `;
        mysql_connection.query(sql_query, (error, results) => {
            if (error) {
                console.error('Error updating the client_session: ', clientSession, error);
                return reject(error);
            } else {
                if (results.length === 0) {
                    console.log('no results for update, but I think its ok');
                    return resolve(clientSession);
                }
                console.log('update seemed to have worked: ', clientSession);
                return resolve(clientSession);
            }
        });
    });
}

function processNameAfterDelay(io, client, clientSession) {
    //client is the socket. 
    //client session is the stored object.
    clientSession.state = "processing";
    console.log("starting processing for ", clientSession);
    client.emit(
        "processing",
        {
            ...clientSession
        }
    );
    updateClientSession(clientSession)
        .then(clientSession => {

            setTimeout(() => {
                //we need to redefine clientSession because it might be out of scope. 
                sendProcessedName(io, clientSession);
            }, processDelay);
        })
        .catch(error => {
            console.log("error updating client session after processing state change: ", error);
        })
}

function sendProcessedName(io, clientSession) {
    console.log("about to get real client session.");
    getClientSession(clientSession.client_id)
        .then(clientSession => {
            clientSession.server_name = shuffleString(clientSession.name);
            clientSession.state = "ready";
            console.log("found real client session.", clientSession);
            return updateClientSession(clientSession);
        })
        .catch(error => {
            console.log("error getting client name.", error);
        })
        .then(clientSession => {
            console.log("about to send processed name back. ", clientSession);
            io.to(clientSession.server_id).emit(
                "name_processed",
                {
                    ...clientSession
                }
            );
            return resolve(clientSession);
        })
        .catch(error => {
            console.log("error sending processed name.", error);
        });
}



main();






