const socketServer = config.socketServer || "";
let testMode = false; //turn on for styling. 
let debug; //determined by querystrings. 
let socket
let this_client_id;
let this_client_index = 0;
let state = "loading";
const el = {};
el.main = document.getElementById('main');
el.local_id = document.getElementById('local_id');
el.remote_id = document.getElementById('remote_id');
el.server_name = document.getElementById('server_name');
el.local_name = document.getElementById('local_name');
el.remember = document.getElementById('remember');
el.error = document.getElementById('error');
el.dont_remember = document.getElementById('dont_remember');
el.btn_submit = document.getElementById('btn_submit');
el.input_name = document.getElementById('input_name');
el.remote_state = document.getElementById('remote_state');


const main = () => {
    setupConsole();
    initSocketConnection();
    setupBtns();
    setupInitialState();
}

const setupBtns = () => {
    el.btn_submit.addEventListener('click', (e) => {
        e.preventDefault();
        sendIdentity();
    });
}


const setupInitialState = () => {
    hide(el.remember);
}

const hide = (_el) => {
    if (_el) {
        _el.style.display = 'none';
    } else {
        console.log(_el + " does not exist.");
    }
}

const show = (_el) => {
    if (_el) {
        _el.style.display = 'block';
    } else {
        console.log(_el + " does not exist.");
    }
}


const showError = (errorMessage) => {
    el.error.innerHTML = errorMessage;
}

const sendIdentity = () => {
    let name = "unset";
    if (!isNullOrEmpty(el.input_name.value)) {
        name = el.input_name.value;
    }
    console.log("sending name... ", name);
    socket.emit("submit_name", {
        id: this_client_id,
        name
    });
}

function isNullOrEmpty(str) {
    return str === null || str.length === 0;
}

const initSocketConnection = () => {
    console.log("attempting socket connection");
    socket = io(socketServer, {
        secure: true,
        query: {
            client_id: sessionStorage.getItem('client_id')
        },
    });

    socket.on("connect", () => {
        console.log("socket.io connected to " + socketServer);
    });

    socket.on("disconnect", () => {
        console.log("disconnected, but everything will be ok.");
    });

    socket.on("connect_failed", (e) => {
        console.log("connect_failed");
        state = "error";
        showError(e);
    });

    socket.on("error", (e) => {
        console.log("error: " + e);
        state = "error";
        showError(e);
    });

    socket.on("introduction", (payload) => {
        console.log("being introduced to server: ", payload);
        this_client_id = payload.socket_id;
        sessionStorage.setItem('client_id', payload.client_id);
        displayPayloadInformation(payload);
    });

    socket.on("welcome_back", (payload) => {
        console.log("being welcomed back from the server: ", payload);
        displayPayloadInformation(payload);
        show(el.remember);
    });

    socket.on("processing", (payload) => {
        console.log("processing: ", payload);
        displayPayloadInformation(payload);
    });

    socket.on("name_processed", (payload) => {
        console.log("name processed: ", payload);
        displayPayloadInformation(payload);
        show(el.remember);
    });
}

const displayPayloadInformation = (payload) => {
    el.server_name.innerHTML = payload.server_name;
    el.local_name.innerHTML = payload.name;
    el.local_id.innerHTML = sessionStorage.getItem('client_id');
    el.remote_id.innerHTML = payload.client_id; //yes it says client id but its matching. 
    el.remote_state.innerHTML = payload.state;
}

const displaySessionStorage = () => {
    el.local_id.innerHTML = sessionStorage.getItem('client_id');
}

const addToConsole = (_string) => {
    el.console.innerHTML += "<br>" + _string;
}

const setupConsole = () => {
    const urlSearchParams = new URLSearchParams(window.location.search);
    const params = Object.fromEntries(urlSearchParams.entries());
    if (params.debug) {
        debug = true;
    }
    if (debug && "console" in window) {
        methods = [
            "log", "assert", "clear", "count",
            "debug", "dir", "dirxml", "error",
            "exception", "group", "groupCollapsed",
            "groupEnd", "info", "profile", "profileEnd",
            "table", "time", "timeEnd", "timeStamp",
            "trace", "warn"
        ];

        generateNewMethod = function (oldCallback, methodName) {
            return function () {
                var args;
                addToConsole(methodName + ":" + arguments[0]);
                args = Array.prototype.slice.call(arguments, 0);
                Function.prototype.apply.call(oldCallback, console, arguments);
            };
        };

        for (i = 0, j = methods.length; i < j; i++) {
            cur = methods[i];
            if (cur in console) {
                old = console[cur];
                console[cur] = generateNewMethod(old, cur);
            }
        }

    }
}



(function () {
    main();
})();




