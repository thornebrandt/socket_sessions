const config = {
    localServer: "https://localhost:8080",
    remoteServer: "https://experiments.thornebrandt.com:8080",
    local: false,
    socketServer: "https://experiments.thornebrandt.com:8080",
    certPath: "/experiments.thornebrandt.com/certs/"
};