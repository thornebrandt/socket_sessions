function asyncOperation() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve('Async operation completed'); // Fulfills the promise with a value
            console.log('This code will not be executed'); // This code will not be reached
        }, 1000);
    });
}

asyncOperation()
    .then(result => {
        console.log(result); // 'Async operation completed'
        console.log('This code will be executed'); // This code will be reached
    });